import grpc
import argparse

from g2p_pb2 import Grapheme
from g2p_pb2_grpc import G2PStub


class G2PClient(object):
    def __init__(self, remote='127.0.0.1:19002'):
        channel = grpc.insecure_channel(remote)
        self.stub = G2PStub(channel)

    def transliterate(self, sentence_list):
        return self.stub.Transliterate(Grapheme(sentences=sentence_list))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='rule-based Korean g2p client')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='127.0.0.1:19002')
    parser.add_argument('-i', '--input',
                        nargs='+',
                        dest='input',
                        help='input Korean',
                        type=str,
                        required=True)
    args = parser.parse_args()

    client = G2PClient(args.remote)

    phoneme = client.transliterate(args.input)
    print(phoneme.sentences)
