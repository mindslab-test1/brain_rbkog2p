# `rbkog2p`: rule-based koG2P

- `brain-g2p-kor-rule-based` 에서 `brain_rbkog2p` 로 변경되었습니다.
- `brain_idl` version: 1.0.0
- 관련 컨플루언스 글: https://pms.maum.ai/confluence/display/audio/rbkog2p%3A+Rule-based+Korean+G2P
- rbkog2p를 grpc로 바로 사용하고자 할 경우, 아래의 "사전 준비하기", "사용해 보기", ... 를 건너뛰어 바로 "Pull from Docker registry and run"을 보면 됩니다.

## 사전 준비하기

사전 정보는 [국립국어원 표준국어대사전](https://stdict.korean.go.kr/)에서 받을 수 있습니다.
로그인한 후, 내 정보 관리 > 사전 내려받기 > 전체 내려받기를 통해 내려받으시면 됩니다.

사전 정보를 tsv로 변환한 후 `dict/` 폴더를 만들고 그곳에 모두 넣습니다.
그런 다음 `python3 preprocess.py`를 통해 사전에서 발음만 추출할 수 있습니다.
만든 파일은 `word_dict.txt`에 추가됩니다.

## 사용해 보기

`python3 process.py`와 같이 아무 인자 없이 시행하면, 여러 가지를 실험할 수 있는 인터랙티브 쉘이 나옵니다.
이곳에서 실제로 문자열이 어떻게 변환될지 알아볼 수 있습니다.

```
>>> 교도통신은 중국-싱가포르간 외교 소식통을 인용해 시 주석이 북미정상회담의 합의 내용 중 한반도 평화협정 체결 내용이 있을 경우 싱가포르를 방문할 계획이었지만, 평화협정 관련 내용이 빠질 것으로 예상되자 싱가포르에 가지 않았다고 설명했습니다.
CLEAN 교도통시는 중국-싱가포르간 외교 소식통을 이뇽해 시 주서기 붕미정상회다메 하븨 내용 중 한반도 평화협쩡 체결 내용이 이쓸 경우 싱가포르를 방문할 계회기얻찌만, 평화협쩡 괄련 내용이 빠질 거스로 예상되자 싱가포르에 가지 아낟따고 설명핻씀니다.
>>> 스위스에서 오셔서 산새들이 속삭이는 산림 숲속의 숫사슴과 샘 속 송사리 새끼를 샅샅이 수색해 식사하고 산 속 고인돌 사이 샘물로 세수하며 살아가는 삼십 삼살 샴쌍둥이 미세스 스미스씨와 미스터 심슨씨는 삼성 설립 사장의 자상한 회사 자산 상속자인 사촌의 사돈이면서 수하물 수송 솜씨를 자랑하는 심삼속씨와 숫기있고 숭글숭글한 숫색시 삼성소속 식산업 종사자 심삼순씨를 만나서 삼성 소속 수산물 운송수송 수속 및 수색 담당 실장에게 스위스에서 숫사슴을 수색했던 것을 인정받아 스위스산 수산물 운송 수송 과정에서 상해 삭힌 냄새가 나는 수산물을 샅샅이 수색해내는 삼성 수산물 운송수송 소속 수색 사원이 되자 산 속을 맨발바닥 두 손바닥 열 발가락 닳게 방방곡곡 뒤져 깊은 숲 산기슭 속 수풀 깊숙이 팔꿈치를 꿈틀꿈틀거려 찾아낸 스위스연방 지도 지리지를 들고 풀숲 샘물 속 수산물을 쉴새없이 수색하다 삼림 산성수 수색 담당 신상순 씨가 실수로 수송한 삼림 산성수 통 삼 톤에 의해 손가락 세 가닥과 열 발가락이 산화되어 응급실 수술실에서 응급수술하게 되었는데 수술실 실수로 쉽사리 수술이 잘 안되어 수술실 식사로 나온 산양삼삼계탕을 삼키지 못하자 심신에 좋다는 산삼을 달인 달콤한 홍삼산삼차를 슈르릅 들이켰더니 힘이 샘물 솟듯이 솟아 신상순 씨의 삼림 산성수 수송 수속 중 일어난 수술 건을 과실치사상죄로 고소하였지만 산성수 수색 담당 신상순 씨의 삼촌인 삼성 소속 식산업 종사자 심삼순씨의 사과로 셋이서 삼삼오오 삼월 삼십일일 세 시 삼십분 삼십 삼초에 쉰 세 살 김식사씨네 시내 스시식당에 시스루 룩으로 식사하러 가서 신선한 샥스핀 스시와 싱싱한 삼색샤시참치스시를 살사소스에 살살 슥삭슥삭 비빈 것과 스위스산 소세지 세 접시를 샤사샥 싹슬어 입속에 쑤셔넣어 살며시 삼키고 스산한 새벽 세시 십삼분 삼십 삼초에 숲 속 산림 산기슭 구석 풀숲 속으로 사라졌다.
CLEAN 스위스에서 오셔서 산쌔드리 속싸기는 살림 숩쏘게 숟싸슴과 샘 속 송사리 새끼를 삳싸치 수새캐 식싸하고 산 속 고인돌 사이 샘물로 세수하며 사라가는 삼십 삼살 샴쌍둥이 미세스 스미스씨와 미스터 심슨씨는 삼성 설립 사장에 자상한 회사 자산 상속짜인 사초네 사도니면서 수하물 수송 솜씨를 자랑하는 심삼속씨와 숟끼읻꼬 숭글숭글한 숟쌕씨 삼성소속 식싸넙 종사자 심삼순씨를 만나서 삼성 소속 수산물 운송수송 수속 믿 수색 담당 실짱에게 스위스에서 숟싸스믈 수새캗떤 거슬 인정바다 스위스산 수산물 운송 수송 과정에서 상해 사킨 냄새가 나는 수산무를 삳싸치 수새캐내는 삼성 수산물 운송수송 소속 수색 사워니 되자 산 소글 맨발빠닥 두 손빠닥 열 발까락 달케 방방곡꼭 뒤져 기픈 숩 산끼슥 속 수풀 깁쑤기 팔꿈치를 꿈틀꿈틀거려 차자낸 스위스연방 지도 지리지를 들고 풀숩 샘물 속 수산무를 쉴쌔업씨 수새카다 삼님 산성수 수색 담당 신상순 씨가 실쑤로 수송한 삼님 산성수 통 삼 토네 의해 손까락 세 가닥꽈 열 발까라기 산화되어 응급씰 수술시레서 응급쑤술하게 되언는데 수술실 실쑤로 쉽싸리 수수리 잘 안되어 수술실 식싸로 나온 사냥삼삼계탕을 삼키지 모타자 심시네 조타는 산사믈 다린 달콤한 홍삼산삼차를 슈르릅 드리켣떠니 히미 샘물 솓뜨시 소사 신상순 씨에 삼님 산성수 수송 수속 중 이러난 수술 거늘 과실치사상죄로 고소하엳찌만 산성수 수색 담당 신상순 씨에 삼초닌 삼성 소속 식싸넙 종사자 심삼순씨에 사과로 세시서 삼사모오 사뭘 삼시빌릴 세 시 삼십뿐 삼십 삼초에 쉰 세 살 김식싸씨네 시내 스시식땅에 시스루 루그로 식싸하러 가서 신선한 샥쓰핀 스시와 싱싱한 삼색쌰시참치스시를 살싸소스에 살살 슥싹쓱싹 비빈 걷꽈 스위스산 소세지 세 접씨를 샤사샥 싹쓰러 입쏘게 쑤셔너어 살며시 삼키고 스산한 새벽 세시 십쌈분 삼십 삼초에 숩 속 살림 산끼슥 구석 풀숩 소그로 사라젿따.
```

변환 과정 중 한 문자열에 대한 수정 사항이 겹칠 수 있는데, `CLEAN`은 이러한 상황이 일어나지 않았다는 뜻입니다.

## Preprocess에 사용해보기

또한 `import process` 이후 위와 같은 역할을 하는 함수 `process.phoneme_process`를 사용해 대용량의 데이터를 처리할 수도 있습니다.

`python3 process.py <input_txt_file>`과 같이 실행하면 `input file`에 대해 g2p 변환이 진행됩니다.

입력 파일은 `\n`으로 구분되어 있는 한국어 문장들입니다.

출력 파일은 입력 파일과 같은 directory에  `<file_name>_g2p.txt`의 이름으로 저장됩니다.
```
INPUT TEXT FILE:
보지 않고 평가하는 건 그 선수에게 실롑니다.
완전히 깎였어요. 완전히 깎였네. 전국 깎기대회야.
...

-----------------
OUTPUT TEXT FILE:
보지 않고 평가하는 건 그 선수에게 실롑니다.   보지 안코 평까하는 건 그 선수에게 실롐니다.
완전히 깎였어요. 완전히 깎였네. 전국 깎기대회야.    완전히 까껴써요. 완전히 까껸네. 전국 깍끼대회야.
...
```

## TTS script Preprocess
TTS script 전처리를 위해서는 `python3 process.py <input_txt_file> --for_tts`와 같이 `--for_tts` option을 붙이면 됩니다.


입력 파일은 `\n`으로 구분되어 있는 `wavpath|text|speaker`의 형태입니다.

출력 파일은 입력 파일과 같은 directory에  `<file_name>_g2p.txt`의 이름으로 저장됩니다.
```
INPUT TEXT FILE:
CWW_Emotional/22k_wav/Sad/0425.wav|그러나 상관하지 않았다.|KDH_SAD
CWW_Emotional/22k_wav/Sad/0490.wav|작품 번호는 삼백십사번이다.|KDH_SAD
...

-----------------
OUTPUT TEXT FILE:
CWW_Emotional/22k_wav/Sad/0425.wav|그러나 상관하지 아낟따.|KDH_SAD
CWW_Emotional/22k_wav/Sad/0490.wav|작품 번호는 삼백씹싸버니다.|KDH_SAD
...
```

## Build and push to Docker registry

사내 docker registry에 업로드되어 있기 때문에, 보통은 직접 빌드하지 않고 다음 단계로 넘어가서 바로 사용할 수 있습니다.

직접 빌드해서 docker registry에 업로드할 경우 최종 커밋에 version을 tag로 달아야 합니다.
또한, `brain_idl` submodule 내용까지 모두 clone 후 `git checkout <version>`을 통해 필요한 버전으로 설정해야 합니다.
```bash
docker build -f Dockerfile -t rbkog2p:<version> .
docker tag rbkog2p:<version> docker.maum.ai:443/rbkog2p:<version>
docker push docker.maum.ai:443/rbkog2p:<version>
```

## Pull from Docker registry and run

```bash
docker pull docker.maum.ai:443/rbkog2p:<version>
docker run -d -p 19002:19002 docker.maum.ai:443/rbkog2p:<version>
```

## Example I/O

```bash
>>> python client.py -r 127.0.0.1:19002 -i "나는 사과를 먹었다." "이번 역은 제기동, 제기동 역입니다."
['나는 사과를 머걷따.', '이번 여근 제기동, 제기동 여김니다.']
```

## Speed test

`python server.py` 로 서버를 작동한 뒤, TTS 대본을 이용해 속도 측정.

```bash
>>> python speedtest.py -t <path_to_tts_metadata>
```

===

## 부연설명

`word_dict.txt`를 만들고 나면 발음이 여러 가지가 가능한 경우 `Grapheme Phoneme1 Phoneme2` 형식으로 되어 있을겁니다. 이 때 문자열을 발음기호로 변환할 때 가능한 발음 2가지 중 첫번째 것이 사용됩니다.
예를 들어, "국가의 명운을 결정지을"의 경우 "국까에/의 명우늘 결쩡지을"과 같이 2가지가 가능하나 실제 출력은 "국까에 명우늘 결쩡지을"과 같이 됩니다.

`foreign_dict.txt`에는 외래어들과 그에 해당하는 phoneme이 적혀 있습니다. 외래어의 경우 `word_dict.txt`보다 `foreign_dict.txt`에서 먼저 검색된 후 예외처리됩니다.

koG2P 관련하여 필요한 국어 지식은 김영현님의 컨플루언스 포스트를 참고하세요: https://pms.maum.ai/confluence/x/wRWIAQ

## 담당자

김영현 연구원(2020.07 퇴사), 박승원 수석(2020.08.31 기준 현재 담당), 김강욱 연구원
