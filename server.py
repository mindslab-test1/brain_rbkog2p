import time
import grpc
import logging
import argparse
import signal

from concurrent import futures
from grpc_health.v1 import health
from grpc_health.v1 import health_pb2_grpc

from process import load_dict, phoneme_process
from g2p_pb2 import Grapheme, Phoneme
from g2p_pb2_grpc import add_G2PServicer_to_server, G2PServicer


_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class G2PServicerImpl(G2PServicer):
    def __init__(self):
        self.dictionary = load_dict('word_dict.txt')
        self.for_dict = load_dict('foreign_dict.txt')

    def Transliterate(self, request, context):
        try:
            sentence_list = request.sentences
            logging.debug('request:%s', sentence_list)
            output_list = list()

            for sentence in sentence_list:
                try:
                    output = phoneme_process(self.dictionary, self.for_dict, sentence)
                except: # when g2p raises an error, set output=input
                    output = sentence
                output_list.append(output)
            logging.debug('response:%s', output_list)
            return Phoneme(sentences=output_list)

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='rule-based Korean g2p client')
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=19002)

    args = parser.parse_args()

    g2p_servicer = G2PServicerImpl()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1),)
    add_G2PServicer_to_server(g2p_servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    health_servicer = health.HealthServicer()
    health_pb2_grpc.add_HealthServicer_to_server(health_servicer, server)
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    def exit_gracefully(signum, frame):
        health_servicer.enter_graceful_shutdown()
        server.stop(60)
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    logging.info('g2p starting at 0.0.0.0:%d', args.port)

    server.wait_for_termination()
