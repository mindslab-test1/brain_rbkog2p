from kiwipiepy import Kiwi, Option
import hangeul

import traceback

class TwoDimArray():
	def __init__(self):
		self.d = {}

	def __getitem__(self, idx):
		a, b = idx
		if a == 0 or b == 0:
			return max(a, b)
		return self.d[idx] if idx in self.d else None

	def __setitem__(self, idx, val):
		self.d[idx] = val

class UkkonenArray():
	def __init__(self):
		self.d = {}
		self.pointer = {}
		self.default_value = None

	def get(self, idx):
		a,b = idx
		if b >= 0:
			D = {b - 1: b - 1, b - 2: -9}
		else:
			D = {abs(b) - 1: -1, abs(b) - 2: -9}
		if a in D:
			return D[a]
		return self.d[a][b+a] if a in self.d and 0 <= b+a < len(self.d[a]) else None

	def set(self, idx, val):
		a,b = idx
		if a not in self.d:
			self.d[a] = [0] * (a * 2 + 1)
		self.d[a][b+a] = val

	def dump(self, limit):
		D = TwoDimArray()
		for d in range(-limit, limit + 1):
			now_idx = min(d, 1)
			for e in range(max(abs(d) - 2, -1), limit):
				target = self.get((e, d))
				while now_idx <= target:
					D[now_idx - d, now_idx] = e
					now_idx += 1
		return D

def edit_distance_ukkonen(original, modified):
	C = UkkonenArray()
	e = 0
	target_d = len(modified) - len(original)
	while True:
		end = False
		for d in range(-e, e+1):
			col = max(C.get((e-1, d-1)) + 1, C.get((e-1, d)) + 1, C.get((e-1, d+1)))
			oridx = col + 1 - d
			while 1 <= oridx <= len(original) and 0 <= col < len(modified) and original[oridx - 1] == modified[col]:
				col += 1
				oridx = col + 1 - d
			C.set((e, d), col)
			if d == target_d and col == len(modified):
				end = True
		if end:
			break
		e += 1
	return C, e

def edit_distance(original, modified):
	C, n = edit_distance_ukkonen(original, modified)
	return C.dump(n + 1)

def space_heuristic(original, phoneme_info):
	u = []
	now_target = 0
	idx = 0
	for a, b, c, d in phoneme_info:
		# TODO: KIWI BUG!
		# 'ㄴ', 'ㄹ' 이외에 뭐가 더 들어올지 몰라서 좀 넓게 잡음
		# 어미 'ㅏ'가 있어서 더 넓게 잡음
		if len(a) == 1 and 0x1161 <= ord(a) < 0x1200:
			d = 0
		if now_target != c + d:
			if b[0] != 'E':
				cnt = original[now_target:c+d].count(' ')
				u.append((' ' * cnt, None, None))
				idx += cnt
			now_target = c + d
		l = len(a)
		u.append((a, idx, idx + l))
		idx += l
	res = "".join(x for x, _, _ in u)
	l = len(res) + 1
	starts = [False] * l
	ends = [False] * l
	for _, a, b in u:
		if a is not None:
			starts[a] = True
		if b is not None:
			ends[b] = True
	return res, starts, ends

def get_difference(original, phoneme_info, text):
	_phoneme_info = phoneme_info
	phoneme_info = [(hangeul.denormalize(x), y) for x, y in phoneme_info]
	trans_idx = [0] * (len(original) + 1)
	original_idx = 0
	new_original = []
	for x in original:
		if x == ' ':
			trans_idx[original_idx] += 1
		else:
			new_original.append(x)
			original_idx += 1
	_original = original
	original = "".join(new_original)
	for i in range(1, original_idx + 1):
		trans_idx[i] += trans_idx[i-1]
	del new_original
	del original_idx

	modified = "".join(x for x, y in phoneme_info)
	l = len(modified) + 1
	must_start = [False] * l
	differents = [False] * l
	l = 0
	for x, _ in phoneme_info:
		must_start[l] = True
		l += len(x)
		differents[l] = True
	del l
	# print(' '.join(hangeul.normalize(x) for x in modified))

	D = edit_distance(original, modified)
	a, b = len(original), len(modified)
	r = []

	while not (a == 0 and b == 0):
		same_char = False if a == 0 or b == 0 else original[a-1] == modified[b-1]
		target = D[a, b]
		if same_char and target == D[a-1, b-1]: # same
			direction = 0
		elif target - 1 == D[a-1, b]:
			direction = 1
		elif target - 1 == D[a, b-1]:
			direction = 2
		else:
			direction = 3
		r.append(direction)
		a -= direction != 2
		b -= direction != 1
	del a
	del b

	r = list(reversed(r))

	a_idx, b_idx = 0, 0
	res, pidx = [], 0
	for dir in r:
		if must_start[b_idx]:
			a_old_idx = a_idx - (dir == 2)
			if dir != 1:
				must_start[b_idx] = False
		a_idx += dir != 2
		b_idx += dir != 1
		if differents[b_idx]:
			if a_old_idx >= 0:
				x,y = _phoneme_info[pidx]
				z,_ = phoneme_info[pidx]
				cnt = a_idx - a_old_idx if y[0] == 'S' else len([x for x in original[a_old_idx : a_idx] if 0x1100 <= ord(x) < 0x1200])
				erase = max(0, len(z) - cnt)
				if len(z) > erase: # TODO: KIWI BUG
					res.append((x, y, a_old_idx, a_idx, erase))
			pidx += 1
			differents[b_idx] = False

	while pidx < len(phoneme_info):
		x,y = _phoneme_info[pidx]
		z,_ = phoneme_info[pidx]
		if len(z) > a_idx - a_old_idx:
			x = hangeul.denormalize(z[:a_idx - a_old_idx])
		res.append((x, y, a_old_idx, a_idx))
		pidx += 1
		a_old_idx = a_idx
	return [(a, b, c + trans_idx[c], d + trans_idx[d - 1], e) for a, b, c, d, e in res]

def make_clean(text, phoneme_result):
	r = list(reversed([x for x in phoneme_result if hangeul.complete_hangeul(x)]))
	res = []
	try:
		for x in text:
			if hangeul.complete_hangeul(x):
				a, b, c = hangeul.split(x)
				d, e, f = hangeul.split(r.pop())
				if not (b == e or (b, e) in {(6, 4), (19, 5), (19, 20)}):
					e = b
				res.append(hangeul.construct(d, e, f))
			else:
				res.append(x)
		if r != []:
			raise ValueError(phoneme_result)
	except:
		return phoneme_result
	return "".join(res)

def main():
	print('Loading Kiwi... ', end="", flush=True)
	kiwi = Kiwi(options=Option.LOAD_DEFAULT_DICTIONARY)
	kiwi.load_user_dictionary('./custom-dict.txt')
	kiwi.prepare()
	print('OK.', flush=True)
	while True:
		ed = False
		try:
			text = input('>>> ')
			if text.startswith('e '):
				ed = True
				text = text[2:]
		except EOFError:
			print()
			break
		word_split, _ = kiwi.analyze(text)[0]
		print(word_split)
		dtext = hangeul.denormalize(text)
		if ed:
			dtext = "".join(dtext.split(' '))
			ptext = "".join(a for a, b, c, d in word_split)
			ptext = hangeul.denormalize(ptext)
			C = edit_distance(dtext, ptext)
			print('    i ' + ' '.join('%2d' % x for x in range(len(ptext) + 1)))
			print(' j       ' + ' '.join(hangeul.normalize(x) for x in ptext))
			for i in range(len(dtext) + 1):
				print('%2d %s %s' % (i, '  ' if i == 0 else hangeul.normalize(dtext[i-1]), ' '.join('  ' if C[i,j] is None else ('%2d' % C[i,j]) for j in range(len(ptext) + 1))))
		else:
			word_split = [(a, b) for a, b, c, d in word_split]
			try:
				new_alignment_result = get_difference(dtext, word_split, text)
				print('NEW:', [(a, b, c, d, hangeul.normalize(dtext[c:d])) for a, b, c, d, _ in new_alignment_result])
				alignment_result = alignment(dtext, word_split)
				print('OLD:', [(a, b, c, d, hangeul.normalize(dtext[c:d])) for a, b, c, d in alignment_result])
			except:
				traceback.print_exc()

if __name__ == '__main__':
	from legacy import alignment
	main()
